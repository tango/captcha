module gitea.com/tango/captcha

go 1.12

require (
	gitea.com/lunny/tango v0.6.2
	gitea.com/tango/cache v0.0.0-20200330032101-c35235184e65
	gitea.com/tango/renders v0.0.0-20191027160057-78fc56203eb4
	github.com/unknwon/com v1.0.1
)
